import { store } from '~/store';
import { Creators as ToastActions } from '~/store/ducks/toast';

class Toast {
  constructor() {
    this.duration = 3000;
    this.timeout = null;
  }

  show({
    color, text, infinite, duration,
  }) {
    clearTimeout(this.timeout);
    this.duration = duration;
    store.dispatch(ToastActions.visibleToast({ color, text }));
    if (!infinite) {
      this.timeout = setTimeout(() => {
        store.dispatch(ToastActions.hiddenToast());
        this.duration = 3000;
      }, this.duration);
    }
  }
}

export default new Toast();
