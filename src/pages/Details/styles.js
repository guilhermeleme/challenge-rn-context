import styled from 'styled-components';
import { colors } from '~/styles';

import BackIcon from '~/assets/icons/arrowb.png';

export const Container = styled.View`
  padding-top: 45px;

  flex: 1;
  background: ${colors.primary};
`;

export const BackButton = styled.TouchableOpacity`
  position: absolute;
  width: 20px;
  height: 20px;
  z-index: 99;
  align-items: center;
  justify-content: center;
  top: 58px;
  left: 20px;
`;

export const Icon = styled.Image.attrs({
  resizeMode: 'cover',
  source: BackIcon,
})`
  width: 19px;
`;
