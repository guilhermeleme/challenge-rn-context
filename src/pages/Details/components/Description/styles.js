import styled from 'styled-components/native';

export const Container = styled.ScrollView.attrs({
  contentContainerStyle: {
    paddingHorizontal: 18,
    paddingVertical: 20,
  },
})`
  background: #fff;
  margin-top: 25px;
  flex: 1;
`;
