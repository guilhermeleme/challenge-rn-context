import React from 'react';
import { Dimensions } from 'react-native';
import HTML from 'react-native-render-html';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';

import { Container } from './styles';

const tagsStyles = {
  p: {
    fontFamily: 'Roboto',
    color: '#4F565D',
    fontSize: 14,
    lineHeight: 30,
  },
  b: {
    fontFamily: 'Roboto',
    color: '#4F565D',
    fontSize: 14,
    lineHeight: 30,
  },
};

const baseStyle = {
  fontFamily: 'Roboto',
  fontSize: 14,
  color: '#4F565D',
  lineHeight: 25,
};

const Description = ({ description, loading }) => (
  <Container>
    {loading ? (
      <>
        <ShimmerPlaceHolder autoRun width={500} style={{ marginBottom: 3 }} />
        <ShimmerPlaceHolder autoRun width={450} style={{ marginBottom: 3 }} />
        <ShimmerPlaceHolder autoRun width={350} style={{ marginBottom: 3 }} />
        <ShimmerPlaceHolder autoRun width={250} style={{ marginBottom: 3 }} />
        <ShimmerPlaceHolder autoRun width={150} style={{ marginBottom: 3 }} />
        <ShimmerPlaceHolder autoRun width={50} />
      </>
    ) : (
      <HTML
        html={description}
        imagesMaxWidth={Dimensions.get('window').width}
        baseFontStyle={baseStyle}
        tagsStyles={tagsStyles}
      />
    )}
  </Container>
);

export default Description;
