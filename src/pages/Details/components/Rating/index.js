import React, { useState } from 'react';
import StarRating from 'react-native-star-rating';

const Rating = ({ rating }) => {
  const [star, setStar] = useState(rating);

  function handleRating(rating) {
    setStar(rating);
  }

  return (
    <StarRating
      disabled={false}
      maxStars={5}
      rating={star}
      starSize={17}
      containerStyle={{ width: 95, marginTop: 2 }}
      selectedStar={rating => handleRating(rating)}
    />
  );
};

export default Rating;
