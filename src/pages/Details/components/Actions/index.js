import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';

import {
  Container, ButtonBuy, ButtonBuyText, ButtonLike,
} from './styles';

const Actions = ({ buttonBuy, onPressBuy, loading }) => {
  const [like, setLike] = useState(false);

  function handleLike() {
    setLike(!like);
  }

  function renderButtonBuy() {
    switch (buttonBuy) {
      case 'FREE':
        return null;
      case 'NOT_FOR_SALE':
        return null;
      case 'FOR_SALE':
        return (
          <ButtonBuy onPress={onPressBuy}>
            <ButtonBuyText>BUY</ButtonBuyText>
          </ButtonBuy>
        );
      default:
        return null;
    }
  }

  return (
    <Container>
      {loading ? (
        <>
          <ShimmerPlaceHolder autoRun width={151} height={36} style={{ borderRadius: 100 }} />
        </>
      ) : (
        <>
          {renderButtonBuy()}
          <ButtonLike onPress={handleLike}>
            <Icon name={like ? 'heart' : 'heart-o'} size={20} color="#fff" />
          </ButtonLike>
        </>
      )}
    </Container>
  );
};

export default Actions;
