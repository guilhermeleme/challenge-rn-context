import styled from 'styled-components/native';
import { colors } from '~/styles';

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  padding: 0 18px;
  margin-top: 15px;
`;

export const ButtonBuy = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})`
  background: ${colors.secondary};
  width: 115px;
  height: 35px;
  border-radius: 100px;
  margin-left: auto;

  align-items: center;
  justify-content: center;
  margin-right: 10px;
`;

export const ButtonBuyText = styled.Text`
  font-family: 'Roboto-Bold';
  font-size: 13px;
  color: #fff;
`;

export const ButtonLike = styled.TouchableOpacity.attrs({
  activeOpacity: 0.9,
})`
  width: 36px;
  height: 36px;
  justify-content: center;
  align-items: center;

  background: ${colors.red};
  border-radius: 18px;
`;
