import React, { useState } from 'react';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';

import {
  Container, Left, Cover, Pages, Right, Title, Author, Price, Row,
} from './styles';

import Rating from '../Rating';

const About = ({
  loading, cover, title, author, totalPages, switchPrice, price, rating,
}) => {
  const [isCoverLoaded, setCoverLoaded] = useState(false);

  function handleLoad() {
    setCoverLoaded(true);
  }

  function renderPricesInfo() {
    switch (switchPrice) {
      case 'FREE':
        return <Price small>Free</Price>;
      case 'NOT_FOR_SALE':
        return <Price small>Not for sale</Price>;
      case 'FOR_SALE':
        return <Price small>${price.listPrice.amount}</Price>;
      default:
        return null;
    }
  }

  return (
    <Container>
      <Left>
        {loading ? (
          <>
            <ShimmerPlaceHolder autoRun width={115} height={150} />
            <ShimmerPlaceHolder
              autoRun
              width={50}
              style={{ position: 'absolute', bottom: -40, left: 30 }}
            />
          </>
        ) : (
          <>
            <Cover source={{ uri: cover }} onLoad={handleLoad} />
            {!isCoverLoaded && (
              <ShimmerPlaceHolder
                autoRun
                width={115}
                height={150}
                style={{ position: 'absolute' }}
              />
            )}
            <Pages>{totalPages} pages</Pages>
          </>
        )}
      </Left>
      <Right>
        {loading ? (
          <>
            <ShimmerPlaceHolder autoRun width={250} style={{ marginBottom: 3 }} />
            <ShimmerPlaceHolder autoRun width={200} style={{ marginBottom: 3 }} />
            <ShimmerPlaceHolder autoRun width={100} style={{ marginBottom: 75 }} />
            <ShimmerPlaceHolder autoRun width={250} height={20} />
          </>
        ) : (
          <>
            <Title>{title}</Title>
            <Author>by {author}</Author>
            <Row>
              {renderPricesInfo()}
              <Rating rating={rating} />
            </Row>
          </>
        )}
      </Right>
    </Container>
  );
};

export default About;
