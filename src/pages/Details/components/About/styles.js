import styled, { css } from 'styled-components/native';
import { colors } from '~/styles';

export const Container = styled.View`
  margin-top: 40px;
  padding: 0 18px;
  flex-direction: row;
`;

export const Left = styled.View`
  margin-right: 15px;
  position: relative;
`;

export const Cover = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 115px;
  height: 150px;
`;

export const Pages = styled.Text`
  position: absolute;
  bottom: -40px;
  left: 30px;

  font-family: 'Roboto';
  font-size: 11px;
  color: #9f8b0c;
  text-align: center;
`;

export const Right = styled.View`
  height: 150px;
  flex: 1;
`;

export const Title = styled.Text`
  font-family: 'Roboto-Bold';
  font-size: 20px;
  color: ${colors.black};
`;

export const Author = styled.Text`
  margin-bottom: 15px;
  font-family: 'Roboto';
  font-size: 11px;
  color: #9f8b0c;
`;

export const Price = styled.Text`
  margin-right: 15px;
  font-family: 'Roboto-Bold';
  font-size: 20px;
  color: ${colors.black};

  ${({ small }) => small
    && css`
      font-size: 17px;
    `}
`;

export const Row = styled.View`
  flex-wrap: wrap;
  margin-top: auto;
  flex-direction: row;
  align-items: center;
`;
