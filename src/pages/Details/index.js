import React, { useState, useContext } from 'react';
import { useNavigationParam } from 'react-navigation-hooks';
import { Linking } from 'react-native';

import { withProvider, Context, Creators } from '~/context/books/details';
import { bookDetails } from '~/services/api';

import Title from '~/components/Title';

import About from './components/About';
import Actions from './components/Actions';
import Description from './components/Description';

import { Container, BackButton, Icon } from './styles';

const Details = ({ navigation }) => {
  const id = useNavigationParam('id');
  const [state, dispatch] = useContext(Context);

  async function fetchDetails() {
    try {
      dispatch(Creators.fetchDetails(id));
      const details = await bookDetails(id);
      dispatch(Creators.fetchDetailsSuccess(details.data));
    } catch (error) {
      dispatch(Creators.fetchDetailsError(error.message));
    }
  }

  useState(() => {
    fetchDetails();
  }, []);

  if (state.error) return null;

  return (
    <Container>
      <BackButton onPress={() => navigation.goBack()}>
        <Icon />
      </BackButton>
      <Title text="Pixter Books" />
      <About
        loading={state.loading}
        cover={state.book.volumeInfo.imageLinks.medium}
        totalPages={state.book.volumeInfo.pageCount}
        title={state.book.volumeInfo.title}
        author={state.book.volumeInfo.authors}
        switchPrice={state.book.saleInfo.saleability}
        price={state.book.saleInfo}
        rating={state.book.volumeInfo.averageRating}
      />
      <Actions
        loading={state.loading}
        buttonBuy={state.book.saleInfo.saleability}
        onPressBuy={() => Linking.openURL(state.book.saleInfo.buyLink)}
      />
      <Description loading={state.loading} description={state.book.volumeInfo.description} />
    </Container>
  );
};

export default withProvider(Details);
