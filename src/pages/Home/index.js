import React, { useContext } from 'react';
import { StatusBar } from 'react-native';
import { useNavigation } from 'react-navigation-hooks';

import { Context, refreshData, fetchMoreData } from '~/context/books/home';

import Title from '~/components/Title';
import Thumbnails from '~/components/Thumbnails';
import { colors } from '~/styles';

import { Container, Search, Icon } from './styles';

const Home = () => {
  const [state, dispatch] = useContext(Context);
  const { navigate } = useNavigation();

  return (
    <Container>
      <StatusBar backgroundColor={colors.primary} barStyle="dark-content" />
      <Title text="Pixter Books " />
      <Search onPress={() => navigate('Filter')}>
        <Icon />
      </Search>
      <Thumbnails
        onRefresh={refreshData(dispatch)}
        data={state.books}
        onEndReached={fetchMoreData(state, dispatch)}
      />
    </Container>
  );
};
export default Home;
