import styled from 'styled-components';
import { colors } from '~/styles';

import searchIcon from '~/assets/icons/search.png';

export const Container = styled.View`
  flex: 1;
  padding: 45px 20px 20px 20px;

  background: ${colors.primary};
`;

export const Search = styled.TouchableOpacity`
  position: absolute;
  top: 51px;
  right: 20px;
`;

export const Icon = styled.Image.attrs({
  resizeMode: 'cover',
  source: searchIcon,
})`
  width: 19px;
`;
