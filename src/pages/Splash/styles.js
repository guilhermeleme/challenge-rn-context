import styled from 'styled-components';
import { colors } from '~/styles';
import { ActivityIndicator } from 'react-native';

export const Container = styled.View`
  flex: 1;
  background: ${colors.primary};
  justify-content: center;
  align-items: center;
`;

export const Loader = styled(ActivityIndicator)`
  margin-top: 20px;
`;
