import React, { useEffect, useContext } from 'react';
import { useNavigation } from 'react-navigation-hooks';

import { searchBookVolumes } from '~/services/api';
import { Context, Creators } from '~/context/books/home';

import { Container, Loader } from './styles';

const Splash = () => {
  const [state, dispatch] = useContext(Context);
  const { navigate } = useNavigation();

  async function fetchInitialData() {
    try {
      dispatch(Creators.fetchData());
      const response = await searchBookVolumes('designer');
      const { items: books } = response.data;
      dispatch(Creators.fetchDataSuccess(books));
    } catch (error) {
      dispatch(Creators.fetchDataError(error.message));
    }
  }

  useEffect(() => {
    fetchInitialData();
  }, []);

  useEffect(() => {
    if (!state.loading) {
      navigate('Home');
    }
  }, [state.loading]);

  return (
    <Container>
      <Loader color="#2C2605" size="large" />
    </Container>
  );
};
export default Splash;
