import React, { useContext } from 'react';

import { withProvider, Context, Creators } from '~/context/books/filter';

import Thumbnails from '~/components/Thumbnails';
import { searchBookVolumes } from '~/services/api';

import {
  Container, Top, BackButton, Icon, Field,
} from './styles';

const Filter = ({ navigation }) => {
  const [state, dispatch] = useContext(Context);
  let timeout = null;

  async function handleSearch({ refreshing, term } = { refreshing: false, term: '' }) {
    try {
      const response = await searchBookVolumes(state.term || term, state.offset);
      const { items: books } = response.data;
      if (refreshing) {
        dispatch(Creators.addBooks(books));
      } else {
        dispatch(Creators.searchBookSuccess(books));
      }
    } catch (error) {
      dispatch(Creators.searchBookError(error.message));
      console.log(error.response);
    }
  }

  function handleInputSearch(text) {
    if (!text) return;
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      dispatch(Creators.searchBook(text));
      handleSearch({ refreshing: false, term: text });
    }, 1000);
  }

  return (
    <Container>
      <Top>
        <BackButton onPress={() => navigation.goBack()}>
          <Icon />
        </BackButton>
        <Field autoFocus onChangeText={handleInputSearch} />
      </Top>
      <Thumbnails
        data={state.books}
        onEndReached={() => handleSearch({ refreshing: true })}
        onRefresh={() => handleSearch({ refreshing: true })}
      />
    </Container>
  );
};

export default withProvider(Filter);
