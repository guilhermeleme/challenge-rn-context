import styled from 'styled-components';
import { colors } from '~/styles';

import BackIcon from '~/assets/icons/arrowb.png';

export const Container = styled.View`
  padding: 45px 20px 20px 20px;

  flex: 1;
  background: ${colors.primary};
`;

export const Field = styled.TextInput`
  flex: 1;
  padding: 15px 20px;
  height: 50px;

  font-family: 'Roboto-Bold';
  border-radius: 6px;
  font-size: 17px;
  color: ${colors.black};

  background: #f0d10f;
`;

export const Top = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const BackButton = styled.TouchableOpacity`
  width: 20px;
  height: 20px;
  align-items: center;
  justify-content: center;
  margin-right: 20px;

  z-index: 99;
`;

export const Icon = styled.Image.attrs({
  resizeMode: 'cover',
  source: BackIcon,
})`
  width: 19px;
`;
