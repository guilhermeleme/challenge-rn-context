import React, { createContext, useReducer } from 'react';

const INITIAL_STATE = {
  loading: false,
  error: null,
  term: '',
  books: [],
  offset: 0,
};

export const Context = createContext(INITIAL_STATE);

export const Types = {
  SEARCH_BOOK: '@filter/SEARCH_BOOK',
  SEARCH_BOOK_ERROR: '@filter/SEARCH_BOOK_ERROR',
  SEARCH_BOOK_SUCCESS: '@filter/SEARCH_BOOK_SUCCESS',
  ADD_BOOKS: '@filter/ADD_BOOKS',
};

export const Creators = {
  searchBook: term => ({
    type: Types.SEARCH_BOOK,
    payload: {
      term,
    },
  }),
  searchBookError: error => ({
    type: Types.SEARCH_BOOK_ERROR,
    payload: {
      error,
    },
  }),
  searchBookSuccess: books => ({
    type: Types.SEARCH_BOOK_SUCCESS,
    payload: {
      books,
    },
  }),
  addBooks: books => ({
    type: Types.ADD_BOOKS,
    payload: {
      books,
    },
  }),
};

function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.SEARCH_BOOK:
      return { ...state, loading: true, term: action.payload.term };
    case Types.SEARCH_BOOK_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        books: action.payload.books,
        offset: state.offset + 21,
      };
    case Types.SEARCH_BOOK_ERROR:
      return { ...state, loading: false, error: action.payload.error };
    case Types.ADD_BOOKS:
      return {
        ...state,
        books: [...state.books, ...action.payload.books],
        offset: state.offset + 21,
      };
    default:
      return state;
  }
}

export function withProvider(Component) {
  return function Provider(props) {
    const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
    return (
      <Context.Provider value={[state, dispatch]}>
        <Component {...props} />
      </Context.Provider>
    );
  };
}
