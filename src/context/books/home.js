import React, { useReducer, createContext } from 'react';
import { searchBookVolumes } from '~/services/api';

export const Context = createContext({});

const FIXED_TERM = 'desginer';

const INITIAL_STATE = {
  loading: true,
  error: null,
  books: [],
  offset: 0,
};

export const Types = {
  FETCH_DATA: '@home/@FETCH_DATA',
  FETCH_DATA_SUCCESS: '@home/FETCH_DATA_SUCCESS',
  FETCH_DATA_ERROR: '@home/FETCH_DATA_ERROR',
  LOADER_DATA_MORE: '@home/LOADER_DATA_MORE',
  REFRESH_DATA_SUCCESS: '@home/REFRESH_DATA_SUCCESS',
};

export const Creators = {
  fetchData: () => ({
    type: Types.FETCH_DATA,
  }),

  fetchDataSuccess: books => ({
    type: Types.FETCH_DATA_SUCCESS,
    payload: {
      books,
    },
  }),

  refreshDataSuccess: books => ({
    type: Types.REFRESH_DATA_SUCCESS,
    payload: {
      books,
    },
  }),

  fetchDataError: error => ({
    type: Types.FETCH_DATA_ERROR,
    payload: {
      error,
    },
  }),

  loaderDataMore: books => ({
    type: Types.LOADER_DATA_MORE,
    payload: {
      books,
    },
  }),
};

export function fetchMoreData(state, dispatch) {
  return async () => {
    try {
      const { offset } = state;
      const response = await searchBookVolumes(FIXED_TERM, offset + 20);
      const { items: books } = response.data;
      dispatch(Creators.loaderDataMore(books));
    } catch (error) {
      dispatch(Creators.fetchDataError(error.message));
    }
  };
}

export function refreshData(dispatch) {
  return async () => {
    try {
      const response = await searchBookVolumes(FIXED_TERM);
      const { items: books } = response.data;
      dispatch(Creators.refreshDataSuccess(books));
    } catch (error) {
      dispatch(Creators.fetchDataError(error.message));
    }
  };
}

function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.FETCH_DATA:
      return {
        ...INITIAL_STATE,
        loading: true,
      };
    case Types.FETCH_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        offset: state.offset + 20,
        books: action.payload.books,
      };
    case Types.REFRESH_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        offset: 20,
        books: action.payload.books,
      };
    case Types.LOADER_DATA_MORE:
      return {
        ...state,
        offset: state.offset + 20,
        books: [...state.books, ...action.payload.books],
      };
    case Types.FETCH_DATA_ERROR:
      return { ...state, loading: false, error: action.payload.error };
    default:
      return state;
  }
}

export function withProvider(Component) {
  return function Provider(props) {
    const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
    return (
      <Context.Provider value={[state, dispatch]}>
        <Component {...props} />
      </Context.Provider>
    );
  };
}
