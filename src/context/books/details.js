import React, { createContext, useReducer } from 'react';

const INITIAL_STATE = {
  id: 1,
  error: null,
  loading: false,
  book: {
    volumeInfo: {
      title: '',
      authors: [],
      publisher: '',
      pageCount: '',
      description: '',
      averageRating: '',
      imageLinks: {
        medium: '',
      },
    },
    saleInfo: {
      saleability: '',
      buyLink: '',
      listPrice: {
        amount: '',
      },
    },
  },
};

export const Context = createContext(INITIAL_STATE);

export const Types = {
  FETCH_DETAILS: '@fetch/FETCH_DETAILS',
  FETCH_DETAILS_ERROR: '@fetch/FETCH_DETAILS_ERROR',
  FETCH_DETAILS_SUCCESS: '@fetch/FETCH_DETAILS_SUCCESS',
};

export const Creators = {
  fetchDetails: id => ({
    type: Types.FETCH_DETAILS,
    payload: {
      id,
    },
  }),
  fetchDetailsError: error => ({
    type: Types.FETCH_DETAILS_ERROR,
    payload: {
      error,
    },
  }),
  fetchDetailsSuccess: details => ({
    type: Types.FETCH_DETAILS_SUCCESS,
    payload: {
      details,
    },
  }),
};

function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.FETCH_DETAILS:
      return { ...state, loading: true };
    case Types.FETCH_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        book: action.payload.details,
      };
    case Types.FETCH_DETAILS_ERROR:
      return { ...state, loading: false, error: action.payload.error };
    default:
      return state;
  }
}

export function withProvider(Component) {
  return function Provider(props) {
    const [state, dispatch] = useReducer(reducer, INITIAL_STATE);
    return (
      <Context.Provider value={[state, dispatch]}>
        <Component {...props} />
      </Context.Provider>
    );
  };
}
