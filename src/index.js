import React from 'react';

import { withProvider } from '~/context/books/home';

import Routes from '~/routes';

const App = () => <Routes />;

export default withProvider(App);
