export default {
  primary: '#FFDD0D',
  secondary: '#4A90E2',

  red: '#DC4B5D',
  black: '#2C2605',
  gray: '#4F565D',
};
