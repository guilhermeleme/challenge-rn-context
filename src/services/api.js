import axios from 'axios';

const api = axios.create({
  baseURL: 'https://www.googleapis.com/books/v1/',
});

export function searchBookVolumes(search, offset = 0) {
  return api.get('/volumes', {
    params: {
      q: search,
      startIndex: offset,
      maxResults: 20,
    },
  });
}

export function bookDetails(id) {
  return api.get(`/volumes/${id}`);
}

export default api;
