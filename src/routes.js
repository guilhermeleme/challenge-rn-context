import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';

import Splash from '~/pages/Splash';
import Home from '~/pages/Home';
import Details from '~/pages/Details';
import Filter from '~/pages/Filter';

const AppNavigator = createStackNavigator(
  {
    Home,
    Details,
    Filter,
  },
  {
    initialRouteName: 'Home',
    headerBackTitleVisible: false,
    defaultNavigationOptions: () => ({
      header: null,
    }),
  },
);

const Routes = createAppContainer(
  createSwitchNavigator(
    {
      App: AppNavigator,
      Splash,
    },
    {
      initialRouteName: 'Splash',
    },
  ),
);

export default Routes;
