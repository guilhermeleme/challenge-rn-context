import React, { useState } from 'react';
import { RefreshControl } from 'react-native';
import { useNavigation } from 'react-navigation-hooks';

import { Container, Item, Cover } from './styles';

import cover from '~/assets/images/cover-default.jpg';

const Thumbnails = ({
  onRefresh, onEndReached, data, ...props
}) => {
  const [loading, setLoading] = useState(false);
  const { navigate } = useNavigation();

  return (
    <Container
      refreshControl={<RefreshControl refreshing={loading} onRefresh={onRefresh} />}
      data={data}
      {...props}
      onEndReached={onEndReached}
      keyExtractor={books => books.id}
      renderItem={({ item }) => (
        <Item
          key={item.id}
          onPress={() => navigate({
            routeName: 'Details',
            params: {
              id: item.id,
            },
          })
          }
        >
          {item.volumeInfo.imageLinks ? (
            <Cover source={{ uri: item.volumeInfo.imageLinks.thumbnail }} />
          ) : (
            <Cover source={cover} />
          )}
        </Item>
      )}
    />
  );
};

export default Thumbnails;
