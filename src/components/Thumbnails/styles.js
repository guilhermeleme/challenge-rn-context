import styled from 'styled-components';
import { FlatList } from 'react-native';

export const Container = styled(FlatList).attrs({
  showsVerticalScrollIndicator: false,
  numColumns: 3,
  onEndReachedThreshold: 0.1,
})`
  margin-top: 40px;
`;

export const Item = styled.TouchableOpacity`
  margin-bottom: 30px;
  margin-right: 20px;
`;

export const Cover = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 115px;
  height: 150px;
`;
