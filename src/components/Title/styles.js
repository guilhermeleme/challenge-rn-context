import styled from 'styled-components';
import { colors } from '~/styles';

export const Container = styled.View`
  position: relative;
  /* margin-bottom: 45px; */
  margin-bottom: ${props => (props.marginBottom ? [props.marginBottom] : '0')};
`;

export const Text = styled.Text`
  text-align: center;
  font-family: 'Roboto';
  font-size: 20px;
  color: ${colors.black};
`;

export const Border = styled.View`
  position: absolute;
  left: 50%;
  bottom: -20px;

  width: 130px;
  height: 2px;
  margin-left: -65px;

  background: #f0d10f;
`;
