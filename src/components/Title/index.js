import React from 'react';

import { Container, Text, Border } from './styles';

const Title = ({ text, marginBottom }) => (
  <Container marginBottom={marginBottom}>
    <Text>{text}</Text>
    <Border />
  </Container>
);

export default Title;
